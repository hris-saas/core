<?php

return [

    'delete_resource_successful' => 'Record deletion succeeded.',
    'delete_resource_failed' => 'Record deletion failed.',
];
