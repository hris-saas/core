<?php

namespace HRis\Core\Generators;

interface Generator
{
    public function generate(): string;
}
